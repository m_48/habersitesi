﻿using LastGamePortal.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LastGamePortal.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index(int? Category,bool? Likeds,bool? Read)
        {
            ViewBag.Sliders = db.Sliders.ToList();
            if (Category!=null)
            {
                Category category = db.Categories.Where(m=>m.Id== Category).FirstOrDefault();
                ViewBag.News = db.News.Where(m=>m.Category.Id==Category).ToList();
                ViewBag.PageTitle =category.Title;
            }
            else if (Likeds==true)
            {
                ViewBag.News = db.News.OrderByDescending(m=>m.LikeCount).ToList();
                ViewBag.PageTitle ="En Çok Beğenilen Haberler";
            }
            else if (Read==true)
            {
                ViewBag.News = db.News.OrderByDescending(m => m.Views).ToList();
                ViewBag.PageTitle = "En Çok Okunan Haberler";
            }
            else
            {
                ViewBag.News = db.News.ToList();
                ViewBag.PageTitle = "Ana Sayfa";
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.LikedList = db.Likes.Where(m=>m.LikedUser.Id==User.Identity.GetUserId());
            }
            List<Liked> ls = new List<Liked>();
            ls = db.Likes.ToList();
            ViewBag.LikedList = ls;
            ViewBag.Categories = db.Categories.ToList();


            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult PostDetails(int Id)
        {
            if (Id>0)
            {
                News model = db.News.Where(m => m.Id == Id).FirstOrDefault();
                ViewBag.NewsList= db.News.Where(m => m.Category.Id == model.Category.Id).Take(3).ToList();
                db.News.FirstOrDefault(m => m.Id == Id).Views++;
                db.SaveChanges();
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult PostLiked(int PostId)
        {

            string userId = User.Identity.GetUserId();
            News news = db.News.Where(m => m.Id == PostId).FirstOrDefault();
            Liked l = db.Likes.Where(m => m.News.Id == PostId && m.LikedUser.Id == userId).FirstOrDefault();
            if (l!=null)
            {
                db.Likes.Remove(l);
                news.LikeCount--;
            }
            else
            {
                ApplicationUser user = db.Users.Where(m => m.Id == userId).FirstOrDefault();
                Liked liked = new Liked()
                {
                    LikedUser = user,
                    News = news
                };
                db.Likes.Add(liked);
                news.LikeCount++;            
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        public ActionResult AddComment(string commentText, int PostId)
        {
            string userId = User.Identity.GetUserId();
            ApplicationUser us = db.Users.Where(m=>m.Id==userId).FirstOrDefault();
            News news = db.News.Where(m => m.Id == PostId).FirstOrDefault();
            Comment com = new Comment {
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                ModifiedUsername = User.Identity.GetUserName(),
                News = news,
                Text = commentText,
                Ower = us
            };
            db.Comments.Add(com);
            db.SaveChanges();

            return RedirectToAction("PostDetails",new {Id=PostId });
        }

    }
}