﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LastGamePortal.Startup))]
namespace LastGamePortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
