namespace LastGamePortal.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LastGamePortal.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(LastGamePortal.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            if (!context.Roles.Any(a => a.Name == "Y�netici"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);
                var role = new ApplicationRole { Name = "Y�netici", Description = "Y�netici" };
                manager.Create(role);
            }
            if (!context.Roles.Any(a => a.Name == "Edit�r"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);
                var role = new ApplicationRole { Name = "Edit�r", Description = "Edit�r" };
                manager.Create(role);
            }
            if (!context.Roles.Any(a => a.Name == "�ye"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);
                var role = new ApplicationRole { Name = "�ye", Description = "�ye" };
                manager.Create(role);
            }
        }
        public class ApplicationRole : IdentityRole
        {

            public string Description { get; set; }

            public ApplicationRole()
            {

            }

            public ApplicationRole(string RoleName, string description)
            {
                this.Description = description;
            }

        }
    }
}
