﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LastGamePortal.Areas.Dashboard.Models
{
 
    public class Sliders
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        [Required,StringLength(20),DisplayName("Slider Başlığı")]
        public string Title { get; set; }

        [StringLength(50),DisplayName("Slider Yazısı")]
        public string Text { get; set; }
        public string Image { get; set; }
    }
}