﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LastGamePortal.Models;
using Microsoft.AspNet.Identity;

namespace LastGamePortal.Areas.Dashboard.Controllers
{

    [Authorize(Roles = "Yönetici,Editör")]
    public class NewsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
         
            var news = db.News.Include(n => n.Category);
            return View(news.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,ShortText,Title,Text,IsDraft,LikeCount,Views,CategoryId,Image,CreatedOn,ModifiedOn,ModifiedUsername")] News news)
        {

            news.CreatedOn = DateTime.Now;
            news.ModifiedOn = DateTime.Now;
            news.ModifiedUsername = User.Identity.GetUserName();
            ModelState.Remove("ModifiedUsername");
            if (ModelState.IsValid)
            {
                if (Request.Files["file"]!=null)
                {
                    string filename = Guid.NewGuid().ToString() + "" + Request.Files[0].FileName;
                    string filePath = Path.Combine(Server.MapPath("~/Content/uploads"), Path.GetFileName(filename));
                    Request.Files[0].SaveAs(filePath);
                    news.Image = filename;
                }
                db.News.Add(news);
                db.SaveChanges();
          
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", news.CategoryId);
            return View(news);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", news.CategoryId);
            return View(news);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ShortText,Title,Text,IsDraft,LikeCount,Views,CategoryId,Image,CreatedOn,ModifiedOn,ModifiedUsername")] News news)
        {
            news.CreatedOn = DateTime.Now;
            news.ModifiedOn = DateTime.Now;
            news.ModifiedUsername = User.Identity.GetUserName();
            ModelState.Remove("ModifiedUsername");
            if (ModelState.IsValid)
            {
                if (Request.Files["file"] != null)
                {
                    string filename = Guid.NewGuid().ToString() + "" + Request.Files[0].FileName;
                    string filePath = Path.Combine(Server.MapPath("~/Content/uploads"), Path.GetFileName(filename));
                    Request.Files[0].SaveAs(filePath);
                    news.Image = filename;
                }
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", news.CategoryId);
            return View(news);
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
