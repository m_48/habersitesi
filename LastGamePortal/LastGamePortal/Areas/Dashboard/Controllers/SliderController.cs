﻿using LastGamePortal.Areas.Dashboard.Models;
using LastGamePortal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LastGamePortal.Areas.Dashboard.Controllers
{
    [Authorize(Roles ="Yönetici")]
    public class SliderController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Dashboard/Slider
        public ActionResult Index()
        {
            ViewBag.List = db.Sliders.ToList();
            return View();
        }
        public ActionResult AddSlider(Sliders model,HttpPostedFileBase file)
        {

            
            if (Request.Files.Count > 0)
            {
                string filename = Guid.NewGuid().ToString() + ""+Request.Files[0].FileName;
                string filePath = Path.Combine(Server.MapPath("~/Content/uploads"), Path.GetFileName(filename));
                Request.Files[0].SaveAs(filePath);
                model.Image = filename;
                db.Sliders.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult RemoveSlider(int Id)
        {
            db.Sliders.Remove(db.Sliders.FirstOrDefault(m=>m.Id==Id));
            db.SaveChanges();

            return RedirectToAction("Index");
        }


    }
}