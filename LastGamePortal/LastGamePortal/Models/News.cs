﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LastGamePortal.Models
{
    [Table("News")]
    public class News :MyEntityBase
    {

        [Required, StringLength(60),DisplayName("Haber Başlığı")]
        public string Title { get; set; }
        [Required, DisplayName("Kısa Açıklama")]
        public string ShortText { get; set; }
        [Required,DisplayName("İçerik")]
        [AllowHtml]
        public string Text { get; set; }

        [DisplayName("Görüntlensinmi ?")]
        public bool IsView{ get; set; }

        [DisplayName("Beğeni Sayısı")]
        public int LikeCount { get; set; }

        [DisplayName("Görüntülenme Sayısı")]
        public int Views { get; set; }

        [DisplayName("Kategorisi")]
        public int CategoryId { get; set; }

        [DisplayName("Kapak Fotografı")]
        public string Image { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual  Category Category { get; set; }
        public virtual List<Liked> Likes { get; set; }


        public News()
        {
            Comments = new List<Comment>();
            Likes = new List<Liked>();

        }



    }
}
