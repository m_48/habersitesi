﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastGamePortal.Models
{
    public class MyEntityBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required,DisplayName("Oluşturma Zamanı")]
        public DateTime CreatedOn { get; set; }
        [Required,DisplayName("Düzenleme Zamanı")]
        public DateTime ModifiedOn { get; set; }
        [Required,StringLength(30),DisplayName("Düzenleyen")]
        public string ModifiedUsername { get; set; }
    }
}
