﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastGamePortal.Models
{
    [Table("Categories")]

    public class Category : MyEntityBase
    {
        [Required,StringLength(50),DisplayName("Başlık")]
        public string Title { get; set; }

        [StringLength(150),DisplayName("Açıklaması")]
        public string Description { get; set; }
    }
}
